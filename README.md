# training-copy_pix4D_results

## GIT
Przy pomocy komand:
``` Bash
cd C:\SkySnap_Code
git clone https://gitlab.com/plk29a/training-copy_pix4d_results.git
```
, lub w aplikacjji github desktop.

## Środowiska wirtualne - miniconda
w tym samym oknie cmd:
``` Bash
cd training-copy_pix4d_results
conda create -n training python=3.10
conda activate training
```
Przed uruchomieniem ostatniej komendy upewnij się że na początku linijki wyświetla się nazwa środowiska.
``` Bash
python -m pip install -r requirements.txt
```

## Uruchomienie skryptu.
W cmd z aktywnym środowiskiem możesz wklepać komendę:
``` Bash
python pix4d_copy_results.py
```
, lub dwuklikiem otworzyć skrypt .bat
