CALL activate training
CALL CLS
python.exe pix4d_copy_results.py

@echo off
echo "Script finished. Pausing so you can review the output :)"
echo "Press any key to close the window"
pause >nul
