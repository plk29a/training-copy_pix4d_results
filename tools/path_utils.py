import os
import shutil
from pathlib import Path, PurePath
from glob import glob

# list all images in folders, subfolders, etc
def get_list_of_image_paths(path_to_image_folder):
    image_path_list = []
    for root, dirs, files in os.walk(path_to_image_folder):
        for image_name in files:
            if ((image_name.lower()).endswith('jpg') or (image_name.lower()).endswith('tif')) and not image_name.startswith("."):
                full_path = os.path.join(root, image_name)
                image_path_list.append(full_path)
    return image_path_list


def get_list_of_shp_paths(path_to_layer_folder):
    layer_path_list = []
    for root, dirs, files in os.walk(path_to_layer_folder):
        for layer_name in files:
            if (layer_name.lower()).endswith('shp') and not layer_name.startswith('.'):
                full_path = os.path.join(root, layer_name)
                layer_path_list.append(full_path)
    return layer_path_list


def get_list_of_area_shp_paths(path_to_layer_folder):
    layer_path_list = []
    for root, dirs, files in os.walk(path_to_layer_folder):
        for layer_name in files:
            if (layer_name.lower()).endswith('name.shp') and not layer_name.startswith('.'):
                full_path = os.path.join(root, layer_name)
                layer_path_list.append(full_path)
    return layer_path_list


def get_list_of_las_paths(path_to_layer_folder):
    layer_path_list = []
    for root, dirs, files in os.walk(path_to_layer_folder):
        for layer_name in files:
            if (layer_name.lower()).endswith('las') or (layer_name.lower()).endswith('laz') and not layer_name.startswith('.'):
                full_path = os.path.join(root, layer_name)
                layer_path_list.append(full_path)
    return layer_path_list


def get_list_of_p4d_path(project_dir):
    project_path_list = []
    for root, dirs, files in os.walk(project_dir):
        for image_name in files:
            if (image_name.lower()).endswith('p4d') and not image_name.startswith('.'):
                full_path = os.path.join(root, image_name)
                project_path_list.append(full_path)
    return project_path_list


def get_all_subdirectories(dirname):
    subfolders = [f.path for f in os.scandir(dirname) if f.is_dir()]
    for dirname in list(subfolders):
            subfolders.extend(get_all_subdirectories(dirname))
    return subfolders


def dir_list_specific_folder(head_dir, folder_name):
    pc_paths = list(Path(head_dir).rglob(folder_name))
    return list(map(str, pc_paths))


def listdir_nohidden(path):
    for f in os.listdir(path):
        if not f.startswith('.'):
            yield f


# list all las files in folder
def list_las_files(path):
    files = [os.path.join(path, f) for f in listdir_nohidden(path) if os.path.isfile(os.path.join(path, f)) and (f.lower().endswith('las') or f.lower().endswith('laz'))]
    return files


# list all tif files in folder
def list_tif_files(path):
    files = [os.path.join(path, f) for f in listdir_nohidden(path) if os.path.isfile(os.path.join(path, f)) and f.lower().endswith('tif')]
    return files


# list all tif files in folder
def list_p4d_files(path):
    files = [os.path.join(path, f) for f in listdir_nohidden(path) if os.path.isfile(os.path.join(path, f)) and f.lower().endswith('p4d')]
    return files


# copy file from source to destination
def copy_file(source, destination):
    # Copy the content of source to destination
    try:
        if not os.path.exists(destination):
            shutil.copy(source, destination)
            print(f"File copied from {source} to {destination} successfully.")
        else:
            print(f"File {destination} exists, hence it was not copied.")

    # If source and destination are same
    except shutil.SameFileError:
        print("Source and destination represents the same file.")

    # If there is any permission issue
    except PermissionError:
        print("Permission denied.")

    # For other errors
    except:
        print("Error occurred while copying file.")


def copy_text_from_txt_to_txt(file_from, file_to, mode):
    with open(file_from) as f:
        with open(file_to, mode, encoding='UTF-8') as f1:
            for line in f:
                f1.write(line)


def list_folders_in_directory(folder):
    subfolders = [f.path for f in os.scandir(folder) if f.is_dir()]
    return subfolders


def list_all_subfolders_in_directory(folder):
    subfolders = []
    for root, dirs, files in os.walk(folder):
        if not dirs:
            subfolders.append(root)
    return subfolders


def navigate_path_containing_folder(path, folder_name):
    if not is_patlib_object(path):
        path = Path(path)

    photo_part_id = path.parts.index(folder_name)
    base_dir = Path(*path.parts[:photo_part_id])
    return base_dir


def navigate_path_to_folder(path, folder_name):
    if not is_patlib_object(path):
        path = Path(path)
    photo_part_id = path.parts.index(folder_name)
    base_dir = Path(*path.parts[:photo_part_id+1])
    return base_dir


def is_patlib_object(path):
    if isinstance(path, PurePath):
        return True
