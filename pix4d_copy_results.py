import argparse
import os
import sys

from colorama import Fore, init, Style

from tools import path_utils

init()


def copy_point_cloud_files(base_path, pc_path):
    results_path = os.path.join(base_path, "Results", "_FTP", "2_Chmura_punktow_DSM")
    pc_dir, pc = os.path.split(pc_path)
    pc_renamed = pc.replace('group1_densified_point_cloud', 'chmuraDSM')
    if not os.path.exists(results_path):
        os.makedirs(results_path)
    path_utils.copy_file(pc_path, os.path.join(results_path, pc_renamed))


def copy_orthomosaic_files(base_path, ortho_path):
    results_path = os.path.join(base_path, "Results", "_FTP", "1_Ortofotomapa")
    ortho_dir, ortho = os.path.split(ortho_path)
    ortho_renamed = ortho.replace('transparent_mosaic_group1', 'ortofotomapa')
    if not os.path.exists(results_path):
        os.makedirs(results_path)
    path_utils.copy_file(ortho_path, os.path.join(results_path, ortho_renamed))


def main(base_path):
    folder_pc = 'point_cloud'
    folder_ortho = '2_mosaic'
    pc_paths = path_utils.dir_list_specific_folder(base_path, folder_pc)
    ortho_paths = path_utils.dir_list_specific_folder(base_path, folder_ortho)

    str_pc = '\n\t\t'.join([path.replace(base_path, '').split('\\')[2] for path in pc_paths])
    str_orto = '\n\t\t'.join([path.replace(base_path, '').split('\\')[2] for path in ortho_paths])
    all_file_co = len(pc_paths) + len(ortho_paths)
    print(Fore.GREEN,
          f"Znaleziono {all_file_co} plików: \n\t Chmury punktów: \n\t\t{str_pc} \n\t Ortomozaiki: \n\t\t{str_orto}",
          Style.RESET_ALL
          )

    if pc_paths:
        for pc_path in pc_paths:
            pc_list = path_utils.list_las_files(pc_path)
            for pc in pc_list:
                copy_point_cloud_files(base_path, pc)

    if ortho_paths:
        for ortho_path in ortho_paths:
            ortho_list = path_utils.list_tif_files(ortho_path)
            for ortho in ortho_list:
                copy_orthomosaic_files(base_path, ortho)


def setup_argparser():
    parser = argparse.ArgumentParser()

    # parser.add_argument('-c', dest='cc_path')
    parser.add_argument('-i', dest='project_dir')
    return parser


if __name__ == '__main__':
    argparser = setup_argparser()
    args = argparser.parse_args(sys.argv[1:])

    if (args.project_dir):
        project_dir = args.project_dir
    else:
        print(r'Podaj sciezke do projektu (np. T:\2022-09-27_Unibep_Mszczonow):')
        project_dir = input()

    # project_dir = r"D:\_TESTS\P4D_copy_results"
    main(project_dir)
